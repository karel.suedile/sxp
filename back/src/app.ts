import express from 'express';
import { User } from './classes/user';
import userService from './services/user.service';

const app: express.Express = express();
app.use(express.json());
const port: number = 3000;

app.get('/user/:userId', (req: express.Request, res: express.Response) => {
    try {
        userService.findByUserId(req.params.userId)
            .then((user: User) => {
                if (user) {
                    res.json({wallet: user.wallet});
                } else {
                    res.status(404).send('user not found');
                }
            })
            .catch(err => {
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.post('/user', (req: express.Request, res: express.Response) => {
    try {
        const apiKey: string = req.body.pk;
        const signature: string = req.body.signature;
        const queryString: string = req.body.qs;
        userService.findByToken(apiKey, signature, queryString)
            .then((user: User) => {
                res.send(user.user);
            })
            .catch(err => {
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.post('/send', (req: express.Request, res: express.Response) => {
    try {
        const apiKey: string = req.body.pk;
        const signature: string = req.body.signature;
        const queryString: string = req.body.qs;
        userService.sendMoney(apiKey, signature, queryString)
            .then((user: User) => {
                res.send(user.user);
            })
            .catch(err => {
                console.log(err);
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.post('/token', (req: express.Request, res: express.Response) => {
    try {
        const apiKey: string = req.body.pk;
        const signature: string = req.body.signature;
        const queryString: string = req.body.qs;

        userService.upsert(apiKey, signature, queryString)
            .then((user: User) => {
                res.send(user.user);
            })
            .catch(err => {
                console.log(err);
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.post('/pop-me', (req: express.Request, res: express.Response) => {
    try {
        const apiKey: string = req.body.pk;
        const signature: string = req.body.signature;
        const queryString: string = req.body.qs;
        const lat: number = req.body.lat;
        const lon: number = req.body.lon;
        userService.findByToken(apiKey, signature, queryString)
            .then((user: User) => {
                return userService.updateLocation(user, lat, lon);
            })
            .then((user: User) => {
                res.send('');
            })
            .catch(err => {
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.post('/around-me', (req: express.Request, res: express.Response) => {
    try {
        const apiKey: string = req.body.pk;
        const signature: string = req.body.signature;
        const queryString: string = req.body.qs;

        const lat: number = req.body.lat;
        const lon: number = req.body.lon;
        userService.findByToken(apiKey, signature, queryString)
            .then((user: User) => {
                return userService.findAround(lat, lon, {excludeWallet: user.wallet})
            })
            .then((userList: User[]) => {
                res.json(userList);
            })
            .catch(err => {
                res.status(400).send(err.toString());
            });
    } catch (e) {
        res.status(400).send(e.toString());
    }
});

app.listen(port, undefined, undefined, () => {
    console.log(`server is listening on ${port}`);
});
