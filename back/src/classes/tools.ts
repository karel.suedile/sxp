
export class Tools {
    public static arrayShuffle<T>(array: T[]): void {
        let currentIndex: number = array.length;
        let temporaryValue: T;
        let randomIndex: number;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
    }
}
