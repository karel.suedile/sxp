import moment from 'moment';

export class User {
    public id: number
    public wallet: string;
    public user: string;
    public lat: number;
    public lon: number;
    public updatedAt: moment.Moment;

    public constructor(data: {[index: string]: string}) {
        this.id = Number(data.id);
        this.wallet = data.wallet;
        this.user = data.user;
        this.lat = Number(data.lat);
        this.lon = Number(data.lon);
        this.updatedAt = moment(data.updatedAt, 'YYYY-MM-DD HH:mm:ss');
    }
}
