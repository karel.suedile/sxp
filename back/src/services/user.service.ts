import { User } from '../classes/user';
import { Tools } from '../classes/tools';
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import mysql from 'mysql';

interface FindAroundOptions {
    excludeWallet?: string;
}

class UserService {
    private db: mysql.Connection;

    public constructor() {
        this.db = mysql.createConnection({
            host: 'mysql',
            user: 'crypto',
            password: 'mysqlpassword',
            database : 'crypto'
        });
    }

    public findByToken(apiKey: string, signature: string, queryString: string): Promise<User> {
        return this.getWalletFromToken(apiKey, signature, queryString)
            .then((wallet: string) => this.findByWallet(wallet));
    }

    public findByUserId(userId: string): Promise<User> {
        return this.dbQuery('SELECT * FROM sxp WHERE user = ? LIMIT 1', [userId])
            .then((results: {[index: string]: string}[]) => {
                return results.length > 0 ? new User(results[0]) : null;
            });
    }

    public upsert(apiKey: string, signature: string, queryString: string): Promise<User> {
        return this.getWalletFromToken(apiKey, signature, queryString)
            .then((wallet: string) => {
                return this.dbQuery('SELECT * FROM sxp WHERE wallet = ? LIMIT 1', [wallet])
                    .then((results: {[index: string]: string}[]) => {
                        if (results.length > 0) {
                            return new User(results[0]);
                        } else {
                            return this.generateUserId()
                                .then((userId: string) => {
                                    const sql: string = 'INSERT INTO sxp (user, wallet, lat, lon, updatedAt) VALUES (?, ?, ?, ?, NOW())';
                                    const params: any[] = [userId, wallet, '9999', '9999'];
                                    return this.dbQuery(sql, params)
                                        .then(() => this.findByWallet(wallet));
                                });
                        }
                    });
            });
    }

    public updateLocation(user: User, lat: number, lon: number): Promise<User> {
        return this.dbQuery('UPDATE sxp SET lat = ?, lon = ?, updatedAt = NOW() WHERE id = ?', [lat, lon, user.id])
            .then(() => user);
    }

    public findAround(lat: number, lon: number, options: FindAroundOptions = {}): Promise<User[]> {
        let sql: string = 'SELECT *, ST_Distance_Sphere(point(lat, lon), point(?, ?)) AS dist FROM sxp WHERE 1';
        sql += ' AND updatedAt > DATE_SUB(NOW(), INTERVAL 30 SECOND)';
        const params: any[] = [lat, lon];

        if (options.excludeWallet) {
            sql += ' AND wallet <> ?'
            params.push(options.excludeWallet);
        }
        sql += ' ORDER BY dist ASC LIMIT 10';
        return this.dbQuery(sql, params)
            .then((userList: {[index: string]: string}[]) => {
                return userList.map((userData: {[index: string]: string}) => new User(userData));
            });
    }

    public sendMoney(apiKey: string, signature: string, queryString: string): Promise<unknown> {
        const config: AxiosRequestConfig = {
            headers: {
                'X-MBX-APIKEY': apiKey
            }
        };
        return axios.post('https://api.binance.com/sapi/v1/capital/withdraw/apply?' + queryString + '&signature=' + signature, {}, config)
            .then((xhr: AxiosResponse) => {
                return xhr.data;
            })
            .catch((err: AxiosError) => {
                throw new Error(err.response.data.msg)
            })
    }

    private dbQuery(sql: string, params: any[] = []): Promise<unknown> {
        return new Promise<unknown>((resolve, reject) => {
            this.db.query(sql, params, ((err, results) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(results);
                }
            }));
        });
    }

    private findByWallet(wallet: string): Promise<User> {
        return this.dbQuery('SELECT * FROM sxp WHERE wallet = ? LIMIT 1', [wallet])
            .then((results: {[index: string]: string}[]) => {
                return results.length > 0 ? new User(results[0]) : null;
            });
    }

    private getWalletFromToken(apiKey: string, signature: string, queryString: string): Promise<string> {
        const config: AxiosRequestConfig = {
            headers: {
                'X-MBX-APIKEY': apiKey
            }
        };
        return axios.get('https://api.binance.com/sapi/v1/capital/deposit/address?' + queryString + '&signature=' + signature, config)
            .then((xhr: AxiosResponse) => {
                return xhr.data.address;
            });
    }

    private generateUserId(): Promise<string> {
        const base: string[] = 'ABCDEFGHJKMNPQRSTUVWXYZ23456789'.split('');
        Tools.arrayShuffle<string>(base);
        const userId: string = base.slice(0, 5).join('');

        return this.findByUserId(userId)
            .then((user: User) => {
                if (user) {
                    return this.generateUserId();
                } else {
                    return userId;
                }
            });
    }
}

export default new UserService();
