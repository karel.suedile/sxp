import { PublicKey } from './storage/public-key';
import { SecretKey } from './storage/secret-key';
import axios, { AxiosResponse } from 'axios';

export interface IWalletRequestElements {
    pk: string;
    qs: string;
    signature: string
}

export enum BrowserType {
    Browser,
    WebView
}

export class Utils {
    public static sign(secret: string, message: string): PromiseLike<string> {
        const encoder: TextEncoder = new TextEncoder();
        return crypto.subtle.importKey('raw', encoder.encode(secret), {name: 'HMAC', hash: {name: 'SHA-256'}}, false, ['sign'])
            .then((key: CryptoKey) => crypto.subtle.sign('HMAC', key, encoder.encode(message)))
            .then((signature: ArrayBuffer) => Array.prototype.map.call(new Uint8Array(signature), x => ('00'+x.toString(16)).slice(-2)).join(''));
    }

    public static getWalletRequestElements(publicKey?: string, secretKey?: string): PromiseLike<IWalletRequestElements> {
        const pk: string = publicKey || PublicKey.get();
        const sk: string = secretKey || SecretKey.get();
        const qs: string = 'coin=SXP&recvWindow=60000&timestamp=' + new Date().getTime();
        return Utils.sign(sk, qs)
            .then((signature: string) => ({ pk, qs, signature }));
    }

    public static sendMoney(userId: string, amount: number): Promise<unknown> {
        return axios.get('/api/user/'+userId)
            .then((xhr: AxiosResponse) => {
                const targetWalletAddress: string = xhr.data.wallet;
                const queryString: string = 'coin=SXP&address='+targetWalletAddress+'&amount='+amount+'&recvWindow=60000&timestamp='+new Date().getTime();
                return Utils.sign(SecretKey.get(), queryString)
                    .then((signature: string) => {
                        return axios.post('/api/send', {pk: PublicKey.get(), qs: queryString, signature})
                    });
            });
    }

    public static waitFor(condition: () => boolean, timeoutInMs = 300): Promise<void> {
        return new Promise((resolve) => {
            const internalWaitFor = (_condition: () => boolean, _timeout: number) => _condition() ?
                resolve(void 0) :
                setTimeout(() => internalWaitFor(_condition, _timeout), _timeout);
            internalWaitFor(condition, timeoutInMs);
        });
    }

    public static wait(timeInMs: number): Promise<void> {
        return new Promise<void>(resolve => {
            setTimeout(() => resolve(void 0), timeInMs)
        });
    }

    public static getBrowserType(): BrowserType {
        const frags: string[] = navigator.userAgent.split(/ /g);
        return frags.pop().startsWith('myapp') ? BrowserType.WebView : BrowserType.Browser;
    }

    public static copyToClipboard(text: string): boolean {
        if (window['clipboardData']?.setData) {
            // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
            return window['clipboardData'].setData('Text', text);
        } else if (document.queryCommandSupported && document.queryCommandSupported('copy')) {
            const textarea: HTMLTextAreaElement = document.createElement('textarea');
            textarea.textContent = text;
            textarea.style.position = 'fixed';  // Prevent scrolling to bottom of page in Microsoft Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand('copy');  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn('Copy to clipboard failed.', ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }

}
