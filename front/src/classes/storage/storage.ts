
export abstract class Storage {
    protected static key: string = 'bt';

    public static set(value: string): void {
        localStorage.setItem(this.key, value);
    }

    public static get(): string {
        return localStorage.getItem(this.key);
    }
}
