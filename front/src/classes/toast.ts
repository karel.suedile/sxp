import { notifFailure, notifSuccess } from '../stores/notification';

export class Toast {
    public static success(message): void {
        notifSuccess.set('');
        notifSuccess.set(message);
    }

    public static failure(message): void {
        notifFailure.set('');
        notifFailure.set(message);
    }
}
