import { writable, Writable } from 'svelte/store';

export const notifSuccess: Writable<string> = writable<string>('');
export const notifFailure: Writable<string> = writable<string>('');
