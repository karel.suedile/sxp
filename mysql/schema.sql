CREATE DATABASE crypto;
CREATE USER 'crypto'@'%' IDENTIFIED BY 'mysqlpassword';
GRANT ALL PRIVILEGES ON crypto.* TO 'crypto'@'%' IDENTIFIED BY 'mysqlpassword';
FLUSH PRIVILEGES;
USE crypto;
CREATE TABLE sxp (
    id int(11) unsigned not null auto_increment primary key,
    user varchar(10) not null,
    wallet varchar(255) not null,
    lat varchar(25) not null,
    lon varchar(25) not null,
    updatedAt datetime
);
